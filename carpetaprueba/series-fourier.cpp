

#include <iostream>
#include <cmath>

const double pi=3.141592654;

//Declaracion de funciones
double funa(int n);  //Coeficientes de la base coseno
double funb(int n);  //Coeficientes de la base seno
void Printgnuplot(int M, double h, double x0, double *fx);

int main(){

  int ii, jj, mm;
  double h=0.1, w=1, sum=0;
  int Nmax=50, M=100;
  double  a0, x0, a[Nmax], b[Nmax], x, faux[Nmax], fx[M];

  a0=4;
 
  std::cout << "set term gif animate" <<std::endl;
  std::cout << "set out 'series-fourier.gif'" << std::endl;
  
  x0=0;
  x=x0;
 
  for (mm=2; mm<Nmax; mm++){

    x=x0;
    for (jj=1; jj<M; jj++){
      sum=0;
      for (ii=1; ii<mm; ii++){
	faux[ii]=funa(ii)*cos(ii*w*x)+funb(ii)*sin(ii*w*x);
	
	sum=sum+faux[ii];
      }
      fx[jj]=0.5*a0+sum;// Funcion en series de fourier
      x=x+h;
    }

    Printgnuplot(M, h, x0, &fx[0]);
    Printgnuplot(M, h, x0, &fx[0]);
    Printgnuplot(M, h, x0, &fx[0]);
    
    }

  return 0;
}


double funa(int n){

  double a;
  a=0;
  //a=-(2/pi)*(pow(-1,n)+1)/(pow(n,2)-1);
  //a=(2/(pi*pow(n,2)))*(pow(-1,n)-1);
  return a;
  
} 

double funb(int n){

  double b;
  b=-(4.0/pi)*(pow(-1,n)-1)/n;
  //b=0;
  //b=2.0/n;
  //b=(2.0/(pi*n))*(1-pow(-1,n)-pi*pow(-1,n));
  //b=0;
  return b;

}


//Impresion de la grafica

void Printgnuplot(int M, double h, double x0, double *fx){
  int ii;
  double x;

  std::cout<<"set yrange [-5,5]"<<std::endl; 
  std::cout<<"plot '-' w lp"<<std::endl;

  x=x0;
  for (ii=1; ii<M; ii++){
    std::cout<<x<<"\t"<<fx[ii]<<std::endl;
    x=x+h;
  }
  
  std::cout<<"e"<<std::endl;
  std::cout<<"pause mouse"<<std::endl;
}
  
